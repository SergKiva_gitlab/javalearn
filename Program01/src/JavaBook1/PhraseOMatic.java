package JavaBook1;

public class PhraseOMatic {
    public static void main(String[] args) {
        String[] slWordListOne = {"круглосуточный", "трехзвенный", "30000 футовый", "взаимный", "обоюдый выйгрыш",
                "фронтэнд", "на основе веб-технологий", "проникающий", "умный", "шесть сигм",
                "метод критического пути", "динамичный"};

        String[] slWordListTwo = {"уполномоченный", "трудный", "чистый продукт", "ориентированный", "центральный",
                "распределенный", "кластаризованный", "фирменный", "нестандартный ум", "позиционированный",
                "сетевой", "сфокусированный", "использованный с выгодой", "выровненный", "нацеленный на", "общий",
                "свместный", "ускоренный"};

        String[] slWordListThree = {"процесс", "пункт разгрузки", "выход из положения", "тип структуры", "талант",
                "подход", "уровень завоеванного внимания", "портал", "период времени", "обзор",
                "образец", "пункт следования"};

        char chSpace = ' ';

        int iOneLength = slWordListOne.length;
        int iTwoLength = slWordListOne.length;
        int iThreeLength = slWordListThree.length;

        int iRand1 = (int) (Math.random() * iOneLength);
        int iRand2 = (int) (Math.random() * iTwoLength);
        int iRand3 = (int) (Math.random() * iThreeLength);

        String sPhrase = slWordListOne[iRand1] + chSpace + slWordListTwo[iRand2] + chSpace + slWordListThree[iRand3];

        System.out.println("Все что нам нужно, это " + sPhrase);
    }
}

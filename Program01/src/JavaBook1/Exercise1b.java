package JavaBook1;

/* Variant A
public class Exercise1b {
    public static  void  main(String[] args) {
        int x = 1;

        while (x < 10) {
            if (x > 3) {
                System.out.println("большой икс: " + String.format("%d", x));
            }
            x = x + 1;
        }
    }
}
*/

/* Variant B
public class Exercise1b {
    public static  void  main(String[] args) {
        int x = 5;

        while (x > 1) {
            x = x - 1;
            if (x < 3) {
                System.out.println("маленький икс: " + String.format("%d", x));
            }

        }
    }
}
*/

/* Variant C */
public class Exercise1b {
    public static  void  main(String[] args) {
        int x = 5;

        while (x > 1) {
            x = x - 1;
            if (x < 3) {
                System.out.println("маленький икс: " + String.format("%d", x));
            }

        }
    }
}

package JavaBook1;

public class EchoTestTestDrive {

    public static void main(String[] args) {

        EchoTest e1 = new EchoTest();
        EchoTest e2 = new EchoTest();
        int x = 0;

        while ( x < 4 ) {  // x < 4
            e1.hello();
            e1.count = e1.count + 1;
            if ( x == 3 ) {  // x == 3
                e2.count = e2.count + 1;
            }
            if ( x > 0 ) { // x > 0
                e2.count = e2.count + e1.count;
            }
            x = x + 1;
        }
        System.out.println(e2.count);
    }
}

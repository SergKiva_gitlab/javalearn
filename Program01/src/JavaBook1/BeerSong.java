package JavaBook1;

public class BeerSong {
    public  static void main(String[] args) {
        int beerNum = 99;
        int isuffix = 9;
        char space = ' ';
        String word = "бутылок";

        while (beerNum>0) {
            System.out.println(String.format("%d", beerNum) + space + word + " пива на стене");
            System.out.println(String.format("%d", beerNum) + space + word + " пива.");
            System.out.println("Возьми одну.");
            System.out.println("Пусти по кругу.");
            System.out.println(space);
            beerNum = beerNum -1;
            if (beerNum>9) {
                isuffix = beerNum / 10;
                isuffix = beerNum - isuffix * 10;
            } else {
                isuffix = beerNum;
            }
            if (isuffix == 1) {
                word = "бутылка";
            } else {
                if (isuffix > 1 && isuffix < 5) {
                    word = "бутылки";
                } else {
                    word = "бутылок";
                }
            }

            if (beerNum > 0) {
                System.out.println(String.format("%d", beerNum) + space + word + " пива на стене");
            } else {
                System.out.println("Нет бутылок пива на стене");
            }
        }
    }
}
